import React from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import { Table, Container, Row, Col } from "react-bootstrap";
import Swal from "sweetalert2";
import "sweetalert2/src/sweetalert2.scss";
import { useNavigate } from "react-router-dom";

const CheckoutModal = (props) => {
  const navigate = useNavigate();
  const placeOrderHandler = () => {
    Swal.fire({
      position: "center",
      icon: "success",
      title: "Your Order Placed Successfully!",
      showConfirmButton: false,
      timer: 1500,
    });
    setTimeout(() => {
      navigate("/shop");
    }, 1500);
  };

  return (
    <>
      <Modal
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Proceed To Pay
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Container>
            <Row>
              {props.cartItems.map((el, index) => (
                <>
                  <Col sm={8}>
                    <Table>
                      <tbody>
                        <tr>
                          <td>Title :</td>
                          <td>
                            <strong>{el.title}</strong>
                          </td>
                        </tr>
                        <tr>
                          <td>Category :</td>
                          <td>
                            <strong>{el.category}</strong>
                          </td>
                        </tr>
                        <tr>
                          <td>Price :</td>
                          <td>
                            <strong>{el.price}</strong>
                          </td>
                        </tr>
                        <tr>
                          <td>Rating :</td>
                          <td>
                            <strong>{el.rating.rate}</strong>
                          </td>
                        </tr>
                        <tr>
                          <td>Quantity :</td>
                          <td>
                            <strong>{el.quantity}</strong>
                          </td>
                        </tr>
                        <tr>
                          <td>Coupon :</td>
                          <td>
                            <strong>{el.coupon}</strong>
                          </td>
                        </tr>
                      </tbody>
                    </Table>
                  </Col>
                  <Col sm={4}>
                    <img src={el.image} alt="item-image" width="200px" />
                  </Col>
                </>
              ))}
            </Row>
          </Container>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="warning" onClick={placeOrderHandler}>
            <strong>{`Proceed to Pay : $ ${
              props.priceCount * props.itemCount
            } /-`}</strong>
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default CheckoutModal;
