import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import "../../styles/components-style/component-style.scss";
import Img from "../../assets/images/banner.svg";

const Banner = () => {
  return (
    <>
      <div className="ui_banner">
        <Container>
          <Row>
            <Col md={6} sm={12}>
              <div className="img_wrapper">
                <img src={Img} alt="Banner_Image" width="400px" />
              </div>
            </Col>
            <Col md={6} sm={12} className="banner_contents_wrapper">
              {/* <div> */}
                <div className="circle"></div>
                <div className="circle_small"></div>
                <div className="circle_small_blue"></div>
              {/* </div> */}
              <div className="banner_content">
                <h1 className="banner_title" style={{ textAlign: "left" }}>
                  Our Gallery,
                  <br />
                  Your Dreams!
                </h1>
                <p className="banner_disc">
                  Lorem Ipsum is simply dummy text of the printing and
                  typesetting industry. Lorem Ipsum is simply dummy text of the
                  printing and typesetting industry.
                </p>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
};

export default Banner;
