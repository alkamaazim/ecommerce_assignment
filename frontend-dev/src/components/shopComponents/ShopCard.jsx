import React from "react";
import "../../styles/pages-style/pages-style.scss";
import { Link } from "react-router-dom";

const ShopCard = ({id, title, rating, category, price, image }) => {
  return (
    <>
      <div className="col-md-6 col-sm-12 gy-4">
          <div class="card ui_shop_card_wrapper">
            <p className="top-icon">
              <i className="fa fa-heart"></i>
            </p>
        <Link to={`/productdetails/${id}`}>
            <img src={image} class="card-img-top" alt={title} />
            <div class="card-body">
              <h5 class="card-title">{title}</h5>
              <p>{rating.rate}</p>
              <p>{category}</p>
              <h5>$ {price}</h5>
            </div>
        </Link>
          </div>
      </div>
    </>
  );
};

export default ShopCard;
