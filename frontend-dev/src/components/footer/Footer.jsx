import React from "react";
import "../../styles/components-style/component-style.scss";
import { Container, Row, Col } from "react-bootstrap";
import Footer_Img from "../../assets/images/footer_img.jpg";

const Footer = () => {
  return (
    <>
      <Container className="ui_footer">
        <Row>
          <Col md={4} sm={12}>
            <div className="footer_img">
              <img src={Footer_Img} alt="footer_img" width="350" />
            </div>
          </Col>
          <Col md={4} sm={12}>
            <div className="ui_footer_info">
              <h1>MiniShop.</h1>
              <div className="footer_link">
                <h6>Privacy Policy</h6>
                <h6>Terms and Condition</h6>
              </div>
            </div>
          </Col>
          <Col md={4} sm={12}>
            <div className="social_icons">
              <ul className="footer_icon_list">
                <li><i className="fa-brands fa-facebook"></i></li>
                <li><i className="fa-brands fa-youtube"></i></li>
                <li><i className="fa-brands fa-twitter"></i></li>
                <li><i className="fa-brands fa-instagram"></i></li>
              </ul>
            </div>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default Footer;
