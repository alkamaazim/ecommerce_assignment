import React from "react";
import '../../../styles/components-style/component-style.scss';
import { Container } from "react-bootstrap";

const NewsLetter = () => {
  return (
    <>
      <div className="ui_news_wrapper">
        <Container className="ui_news_letter">
          <div className="news_letter">
            <h1 className="section_title">Join Ours News Letters</h1>
            <p>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s,
            </p>
            <div className="news_letter_search_box">
              <input
                className="form-control me-2 search_box"
                type="search"
                placeholder="Insert your mail here"
                aria-label="Search"
              />
            </div>
          </div>
        </Container>
      </div>
    </>
  );
};

export default NewsLetter;
