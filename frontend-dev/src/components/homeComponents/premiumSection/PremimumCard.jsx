import React from "react";
import { Link } from "react-router-dom";
import { Col } from 'react-bootstrap'

const PremimumCard = ({ id, category, title, image }) => {
  return (
    <Col md={4} sm={12} className="gy-4">
      <div className="card ui_premimum_card">
        <img src={image} className="card-img-top" alt={title} height="270" />
        <div className="card-body">
          <h5 className="card-category">{category}</h5>
          <h3 className="card-title">{title}</h3>
          <Link to={`/productdetails/${id}`} className="btn_link">
            <div className="premimum_btn">
              <i className="fa fa-arrow-right"></i>
            </div>
          </Link>
        </div>
      </div>
    </Col>
  );
};

export default PremimumCard;
