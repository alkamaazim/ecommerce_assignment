import React, { useEffect, useState } from "react";
import { Container, Row } from "react-bootstrap";
import "../../../styles/components-style/component-style.scss";
import PremimumCard from "./PremimumCard";
import { useNavigate } from "react-router-dom";

const PremimumSection = ({ fetchProduct }) => {
  const navigate = useNavigate();
  const [storeItem, setStoreItem] = useState([]);
  const [activeLinkColor, setActiveLinkColor] = useState({
    all_product: "#f86338",
    mens: "",
    womens: "",
    jwelery: "",
    electronics:"",
  });

  useEffect(()=>{
    setStoreItem(fetchProduct);
  }, [fetchProduct])

  const handleLink1 = (e) => {
    if(e.target.value === 0){
      setActiveLinkColor({all_product: "#f86338", mens:"", womens:"", jwelery: "",electronics:""})
      setStoreItem(fetchProduct);
    }
    else if(e.target.value === 1){
      setActiveLinkColor({all_product: "", mens:"#f86338", womens:"", jwelery: "",electronics:""})
      const data = fetchProduct.filter((item)=>  item.category === "men's clothing");
      setStoreItem(data);
    }
    else if(e.target.value === 2){
      setActiveLinkColor({all_product: "", mens:"", womens:"#f86338", jwelery: "",electronics:""})
      const data = fetchProduct.filter((item)=>  item.category === "women's clothing");
      setStoreItem(data);
    }
    else if(e.target.value === 3){
      setActiveLinkColor({all_product: "", mens:"", womens:"", jwelery:"#f86338",electronics:""})
      const data = fetchProduct.filter((item)=>  item.category === "jewelery");
      setStoreItem(data);
    }
    else if(e.target.value === 4){
      setActiveLinkColor({all_product: "", mens:"", womens:"", jwelery:"",electronics:"#f86338"})
      const data = fetchProduct.filter((item)=>  item.category === "electronics");
      setStoreItem(data);
    }
  }

  const handleMoreBtn = () => {
    navigate("/shop");
  } 


  return (
    <>
      <div className="ui-premimum-section">
        <h1 className="section_title">Our Premimum Collection</h1>
        <Container>
          <div className='text_filter_link'>
              <ul className='text_filter_ul'>
                  <li className='txt_link' style={{color: activeLinkColor.all_product}} id="all_products" value="0" onClick={handleLink1}>All Products</li>
                  <li className='txt_link' style={{color: activeLinkColor.mens}} id="mens" value="1" onClick={handleLink1}>Men's clothing</li>
                  <li className='txt_link' style={{color: activeLinkColor.womens}} id="womens" value="2" onClick={handleLink1}>Women's clothing</li>
                  <li className='txt_link' style={{color: activeLinkColor.jwelery}} id="jwelery" value="3" onClick={handleLink1}>Jwelery</li>
                  <li className='txt_link' style={{color: activeLinkColor.electronics}} id="electronics" value="4" onClick={handleLink1}>Electronics</li>
              </ul>
          </div>
          <Row>
            {storeItem.slice(0, 6).map((item) => ( 
              <PremimumCard
                key={item.id}
                id={item.id}
                category={item.category}
                title={item.title}
                image={item.image}
              />
            ))}
          </Row>
          <div className="text-center mt-5">
            <button type="button" className="section_btn" onClick={handleMoreBtn}>Find out more</button>
          </div>
        </Container>
      </div>
    </>
  );
};

export default PremimumSection;
