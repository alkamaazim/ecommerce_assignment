import React from "react";
import "../../../styles/components-style/component-style.scss";

const ArticleCard = () => {
  return (
    <>
      <div className="col-md-6 col-sm-12 gy-4">
        <div className="article_card">
          <div className="card">
            <img src="..." className="card-img-top" alt="..." />
            <div className="card-body">
              <h3 className="card-title">Card title</h3>
              <p className="card-text">
                Some quick example text to build on the card title and make up
                the bulk of the card's content.
              </p>
              {/* <a href="#" className="btn btn-primary">Go somewhere</a> */}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ArticleCard;
