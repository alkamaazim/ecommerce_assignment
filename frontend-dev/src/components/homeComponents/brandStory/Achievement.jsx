import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import achieve_img1 from "../../../assets/images/achieve1.png";
import achieve_img2 from "../../../assets/images/achieve2.png";
import achieve_img3 from "../../../assets/images/achieve3.png";
import achieve_img4 from "../../../assets/images/achieve4.png";
import achieve_img5 from "../../../assets/images/achieve5.png";
import achieve_img6 from "../../../assets/images/achieve6.png";

const Achievement = () => {
  return (
    <>
      <div className="ui_achievement">
        <Container>
          <h1 className="section_title">Our Achievement</h1>
          <Row>
            <Col md={2} sm={12}>
              <div className="achieve_img">
                <img src={achieve_img1} alt="achieve1" />
              </div>
            </Col>
            <Col md={2} sm={12}>
              <div className="achieve_img">
                <img src={achieve_img2} alt="achieve2" />
              </div>
            </Col>
            <Col md={2} sm={12}>
              <div className="achieve_img">
                <img src={achieve_img3} alt="achieve3" />
              </div>
            </Col>
            <Col md={2} sm={12}>
              <div className="achieve_img">
                <img src={achieve_img4} alt="achieve4" />
              </div>
            </Col>
            <Col md={2} sm={12}>
              <div className="achieve_img">
                <img src={achieve_img5} alt="achieve5" />
              </div>
            </Col>
            <Col md={2} sm={12}>
              <div className="achieve_img">
                <img src={achieve_img6} alt="achieve6" />
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
};

export default Achievement;
