import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import "../../../styles/components-style/component-style.scss";
import brandImg from "../../../assets/images/brand_story.jpg";
import Achievement from "./Achievement";
import Article from "./Article";
import NewsLetter from "../newsletter/NewsLetter";

const BrandStory = () => {
  return (
    <>
      <div className="ui_brand_story">
        <Container>
          <Row>
            <Col md={6} sm={12}>
              <div className="left_img_column">
                <img src={brandImg} alt="brand" />
              </div>
            </Col>
            <Col md={6} sm={12}>
              <div className="right_content_column">
                <div>
                  <h1 className="section_title">
                    Story about
                    <br />
                    Our brand
                  </h1>
                  <p>
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry. Lorem Ipsum has been the industry's
                    standard dummy text ever since the 1500s, when an unknown
                    printer took a galley of type and scrambled it to make a
                    type specimen book. It has survived not only five centuries,
                    but also the leap into electronic typesetting, remaining
                    essentially unchanged. It was popularised in the 1960s with
                    the release of Letraset sheets containing Lorem Ipsum
                    passages, and more recently with desktop publishing software
                    like Aldus PageMaker including versions of Lorem Ipsum.
                  </p>
                  <h5>Read full story</h5>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
        <Achievement />
        <Article />
      </div>
      <NewsLetter />
    </>
  );
};

export default BrandStory;
