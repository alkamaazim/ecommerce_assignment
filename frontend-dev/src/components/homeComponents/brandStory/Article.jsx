import React from "react";
import "../../../styles/components-style/component-style.scss";
import { Container, Row } from "react-bootstrap";
import ArticleCard from "./ArticleCard";

const Article = () => {
  return (
    <>
      <div className="ui_article_section">
        <Container>
          <div className="ui_article_container">
            <h1 className="section_title" style={{ textAlign: "left" }}>
              Get Better insights
              <br />
              from Our Articles
            </h1>
            <p>See more</p>
          </div>
          <Row>
            <ArticleCard />
            <ArticleCard />
            <ArticleCard />
            <ArticleCard />
          </Row>
        </Container>
      </div>
    </>
  );
};

export default Article;
