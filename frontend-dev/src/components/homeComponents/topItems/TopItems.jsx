import React from "react";
import "../../../styles/components-style/component-style.scss";

const TopItems = () => {
  return (
    <>
      <div className="ui_top_items">
        <h1 className="section_title">Top Items</h1>
        <div className="head_sub_text">
          <p className="py-3">
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry.
            <br />
            Lorem Ipsum has been the industry's standard dummy text ever since
            the 1500s,
          </p>
        </div>
      </div>
    </>
  );
};

export default TopItems;
