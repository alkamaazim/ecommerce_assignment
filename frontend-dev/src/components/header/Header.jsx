import React from "react";
import "../../styles/components-style/component-style.scss";
import { Container } from "react-bootstrap";
import { Link } from "react-router-dom";
import NavigationMenu from "./NavigationMenu";

const Header = ({cartItems, fetchProduct}) => {
  return (
    <>
      <Container className="ui_head_container">
        <div className="ui_top_head">
          <div className="brand_logo">
            <h3>
              <Link to="/" className="brand_txt">me<span className="brand_span">SHOP.</span></Link>
            </h3>
          </div>
          <div className="top_right_element">
            <ul className="top_right_txt">
              <li><h6><i className="fa fa-phone"></i> Call Center</h6></li>
              <li><h6><i class="fa-solid fa-truck"></i> Shipping & Return</h6></li>
            </ul>
          </div>
        </div>
      </Container>
      <NavigationMenu cartItems={cartItems} fetchProduct={fetchProduct} />
    </>
  );
};

export default Header;
