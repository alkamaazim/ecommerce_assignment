import React from "react";
import { Container, Button } from "react-bootstrap";
import "../../styles/components-style/component-style.scss";
import { Link, useNavigate } from "react-router-dom";

const NavigationMenu = ({ cartItems, fetchProduct }) => {
  const navigate = useNavigate();
  // const [searchedResult, setSearchedResult] = useState("");

  const handleRedirectToCart = () => {
    navigate("/mycart");
  };

  // const handleSearchInput = (e) => {
  //   let searchInput = e.target.value;
  //   if (searchInput !== "") {
  //     fetchProduct.filter((item) =>
  //       setSearchedResult(
  //         item.title.toLowerCase().includes(searchInput.toLowerCase())
  //       )
  //     );
  //   }else{
  //     setSearchedResult("");
  //   }
  // };
  // console.log(searchedResult);

  return (
    <>
      <div className="ui_navigation">
        <nav className="navbar navbar-expand-lg">
          <Container>
            <Button
              className="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </Button>
            <div
              className="collapse navbar-collapse navigation_wrapper"
              id="navbarSupportedContent"
            >
              <div>
                <ul className="navbar-nav me-auto mb-2 mb-lg-0 link_list">
                  <li className="nav-item">
                    <h2>
                      <Link className="nav-link" to="/">
                        Home
                      </Link>
                    </h2>
                  </li>
                  <li className="nav-item">
                    <h2>
                      <Link className="nav-link" to="/shop">
                        Shop
                      </Link>
                    </h2>
                  </li>
                  <li className="nav-item">
                    <h2>
                      <Link className="nav-link" to="/gallery">
                        Gallery
                      </Link>
                    </h2>
                  </li>
                  <li className="nav-item">
                    <h2>
                      <Link className="nav-link">About</Link>
                    </h2>
                  </li>
                </ul>
              </div>

              <div>
                <input
                  className="form-control me-2 search_box"
                  type="search"
                  placeholder="Search what you need"
                  aria-label="Search"
                  // onChange={(e) => handleSearchInput(e)}
                />
                {/* <div>
                  <ul></ul>
                </div> */}
              </div>

              <div>
                <ul className="top_icons">
                  <li>
                    <i className="fa fa-heart icon"></i>
                  </li>
                  <li style={{ cursor: "pointer" }}>
                    <i
                      className="fa fa-cart-shopping icon"
                      style={{ color: "black" }}
                      onClick={handleRedirectToCart}
                    >
                      <sup>{cartItems.length === 0 ? 0 : cartItems.length}</sup>
                    </i>
                  </li>
                  <li>
                    <i className="fa fa-user icon"></i>
                  </li>
                  <li>
                    <i className="fa fa-bell icon"></i>
                  </li>
                </ul>
              </div>
            </div>
          </Container>
        </nav>
      </div>
    </>
  );
};

export default NavigationMenu;
