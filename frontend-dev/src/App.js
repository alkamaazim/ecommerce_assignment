import "./App.css";
import React, { useEffect, useState } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Header from "./components/header/Header";
import Footer from "./components/footer/Footer";
import Home from "./pages/home/Home";
import Shop from "./pages/shop/Shop";
import ProductDetails from "./pages/productDetails/ProductDetails";
import MyCart from "./pages/myCart/MyCart";
import Gallery from "./pages/gallery/Gallery";
import Swal from "sweetalert2";
import "sweetalert2/src/sweetalert2.scss";

function App() {
  const [fetchProduct, setFetchproduct] = useState([]);
  const [cartItems, setcartItems] = useState([]);
  const [itemCount, setItemCount] = useState();
  let productExist = [];

  useEffect(() => {
    fetch("https://fakestoreapi.com/products")
      .then((res) => res.json())
      .then((json) => setFetchproduct(json));
  }, []);

  const handleAddToCart = (product) => {
    Swal.fire({
      position: "center",
      icon: "success",
      title: "Item Added to Cart!",
      showConfirmButton: false,
      timer: 1500,
    });
    productExist = cartItems.find((item) => item.id === productExist.id);
    if (productExist) {
      setcartItems(
        cartItems !== "" &&
          cartItems.map((item) =>
            item.id === product.id
              ? { ...productExist, quantity: productExist.quantity + 1 }
              : item
          )
      );
    } else {
      setcartItems([...cartItems, { ...product, quantity: 1 }]);
    }
  };

  const handleRemoveCartItem = (productID) => {
    Swal.fire({
      position: "center",
      icon: "success",
      title: "Item Removed from Cart!",
      showConfirmButton: false,
      timer: 1500,
    });
    const newList = cartItems.filter((item) => item.id !== productID);
    setcartItems(newList);
  };

  const itemCountFun = (data) => {
    setItemCount(data);
  };

  const handleDublicateItem = (data) => {
    return cartItems.filter((item) => item.id !== data);
  };

  return (
    <>
      <BrowserRouter>
        <Header cartItems={cartItems} fetchProduct={fetchProduct} />
        <Routes>
          <Route
            exact
            path="/"
            element={<Home fetchProduct={fetchProduct} />}
          />
          <Route
            exact
            path="/shop"
            element={<Shop fetchProduct={fetchProduct} />}
          />
          <Route
            exact
            path="/productdetails/:id"
            element={
              <ProductDetails
                handleAddToCart={handleAddToCart}
                itemCountFun={itemCountFun}
                handleDublicateItem={handleDublicateItem}
              />
            }
          />
          <Route
            exact
            path="/mycart"
            element={
              <MyCart
                cartItems={cartItems}
                handleAddToCart={handleAddToCart}
                handleRemoveCartItem={handleRemoveCartItem}
                itemCount={itemCount}
              />
            }
          />
          <Route exact path="/gallery" element={<Gallery />} />
        </Routes>
        <Footer />
      </BrowserRouter>
    </>
  );
}

export default App;
