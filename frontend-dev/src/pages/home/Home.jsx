import React from "react";
import PremimumSection from "../../components/homeComponents/premiumSection/PremimumSection";
import TopItems from "../../components/homeComponents/topItems/TopItems";
import BrandStory from "../../components/homeComponents/brandStory/BrandStory";
import Banner from "../../components/banner/Banner.jsx";

const Home = ({fetchProduct}) => {

  // const [fetchProduct, setFetchproduct] = useState([]);

  // useEffect(() => {
  //   fetch("https://fakestoreapi.com/products")
  //     .then((res) => res.json())
  //     .then((json) => setFetchproduct(json));
  // }, []);

  return (
    <>
      <Banner />
      <PremimumSection fetchProduct={fetchProduct} />
      <TopItems />
      <BrandStory />
    </>
  );
};

export default Home;
