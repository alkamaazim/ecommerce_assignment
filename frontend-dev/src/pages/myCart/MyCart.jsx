import React, { useState } from "react";
import "../../styles/pages-style/pages-style.scss";
import { Container, Row, Col, Button } from "react-bootstrap";
import CartList from "./CartList";
import TopItems from "../../components/homeComponents/topItems/TopItems";
import NewsLetter from "../../components/homeComponents/newsletter/NewsLetter";
import CheckoutModal from "../../components/cart/modal";

const MyCart = ({ cartItems, handleRemoveCartItem, itemCount }) => {
  let priceCount = 0;
  const [modalShow, setModalShow] = React.useState(false);
  const [getCoupon, setGetCoupon] = useState("No coupon apply");
  const [allCartItems, setAllCartItems] = useState(cartItems);

  const checkoutHandler = () => {
    setModalShow(true)
  }

  const onChangeHandler = (value) => {
    setGetCoupon(value);
  }
  const submitHandler = (e) => {
    e.preventDefault();
    setAllCartItems(
      {...cartItems, getCoupon}
      // coupon: getCoupon
    )
  }

  console.log(allCartItems)

  return (
    <>
      <div className="ui_my_cart">
        <Container>
          <h1 className="section_title" style={{ textAlign: "left" }}>
            My Cart
          </h1>
          <Row className="mt-5">
            {cartItems.length === 0 && <div>No item are added.</div>}
            <Col md={7} sm={12}>
              <CartList
                cartItems={cartItems}
                handleRemoveCartItem={handleRemoveCartItem}
                itemCount={itemCount}
              />
            </Col>
            <Col md={5} sm={12}>
              <div className="coupon_container">
                <div className="card">
                  <h4>Have a Coupon?</h4>
                  <form onSubmit={submitHandler}>
                    <input type="text" placeholder="Coupon Code" onChange={(e)=>onChangeHandler(e.target.value)} />
                    <Button className="coupon_btn" type="submit">Apply</Button>
                  </form>
                </div>
              </div>
              <div className="cart_total_wrapper mt-5">
                <h4>Cart Totals</h4>
                <div className="cart_total mt-4">
                  <div className="left_start_title">
                    <p>Subtotal</p>
                    <p>Shipping</p>
                    <p>Total</p>
                  </div>
                  <div className="left_start_title">
                    {cartItems.map((item) => (
                      <p style={{ display: "none" }}>
                        {(priceCount += item.price)}
                      </p>
                    ))}
                    <p>$ {priceCount * itemCount}</p>
                    <p>Free shipping</p>
                    <p>$ {priceCount * itemCount}</p>
                  </div>
                </div>
                <Button className="checkout_btn" onClick={checkoutHandler}>Checkout</Button>
                <CheckoutModal show={modalShow} onHide={() => setModalShow(false)} cartItems={allCartItems} priceCount={priceCount} itemCount={itemCount}  />
              </div>
            </Col>
          </Row>
        </Container>
      </div>
      <TopItems />
      <NewsLetter />
    </>
  );
};

export default MyCart;
