import React from "react";
import "../../styles/pages-style/pages-style.scss";
import { Container, Row, Col, Button } from "react-bootstrap";

const CartList = ({ cartItems, handleRemoveCartItem, itemCount }) => {
  return (
    <>
      <div className="ui_cart_list">
        <Container>
          <Row>
            {cartItems.map((item) => (
              <>
                <Col sm={4} className="gy-4">
                  <div className="cartList_left_col">
                    <div className="card">
                      <img src={item.image} alt="" height="180" />
                    </div>
                  </div>
                </Col>
                <Col sm={8} className="gy-4">
                  <div className="cartList_right_col">
                    <h5>{item.title}</h5>
                    <div className="product_info">
                      <p>
                        Size
                        <br />
                        Quantity
                      </p>
                      <p>
                        :<br />:
                      </p>
                      <p>
                        M<br />
                        {itemCount}
                      </p>
                    </div>
                    <div className="detail_info">
                      <h1 className="product_price">${item.price}</h1>
                      <div>
                        <Button
                          type="button"
                          className="cart-btn"
                          onClick={()=>handleRemoveCartItem(item.id)}
                        >
                          <i class="fa-solid fa-trash"></i>
                        </Button>
                        &nbsp;&nbsp;
                        <Button type="button" className="cart-btn">
                          wishlist
                        </Button>
                      </div>
                    </div>
                  </div>
                </Col>
              </>
            ))}
          </Row>
        </Container>
      </div>
    </>
  );
};

export default CartList;
