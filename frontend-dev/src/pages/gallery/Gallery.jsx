import React from "react";
import Banner from "../../components/banner/Banner";
import NewsLetter from "../../components/homeComponents/newsletter/NewsLetter";

const Gallery = () => {
  return (
    <>
      <Banner />
      <NewsLetter />
    </>
  );
};

export default Gallery;
