import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import "../../styles/pages-style/pages-style.scss";
import Img from "../../assets/images/brand_story.jpg";
import Review from "./Review";

const ProductDescription = () => {
  return (
    <>
      <div className="ui_product_description">
        <Container>
          <Row>
            <Col md={6} sm={12}>
              <div className="desc_left_column">
                <h3 className="desc_title mb-4">Description</h3>
                <p className="desc_content">
                  Lorem Ipsum is simply dummy text of the printing and
                  typesetting industry. Lorem Ipsum has been the industry's
                  standard dummy text ever since the 1500s, when an unknown
                  printer took a galley of type and scrambled it to make a type
                  specimen book. It has survived not only five centuries, but
                  also the leap into electronic typesetting, remaining
                  essentially unchanged. It was popularised in the 1960s with
                  the release of Letraset sheets containing Lorem Ipsum
                  passages, and more recently with desktop publishing software
                  like Aldus PageMaker including versions of Lorem Ipsum.
                  <br/><br/>
                  Lorem Ipsum is simply dummy text of the printing and
                  typesetting industry. Lorem Ipsum has been the industry's
                  standard dummy text ever since the 1500s, when an unknown
                  printer took a galley of type and scrambled it to make a type
                  specimen book.  
                </p>
                <Review />
              </div>
            </Col>
            <Col md={6} sm={12}>
              <div className="desc_right_column">
                <img src={Img} alt="" width="500" />
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
};

export default ProductDescription;
