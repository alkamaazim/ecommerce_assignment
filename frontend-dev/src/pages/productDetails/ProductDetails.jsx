import React, { useEffect, useState } from "react";
import "../../styles/pages-style/pages-style.scss";
import { Container, Row, Col, Button } from "react-bootstrap";
import ProductDescription from "./ProductDescription";
import { useParams } from "react-router-dom";
import NewsLetter from "../../components/homeComponents/newsletter/NewsLetter";
import { DynamicStar } from "react-dynamic-star";

const ProductDetails = ({ handleAddToCart, itemCountFun, handleDublicateItem }) => {
  const { id } = useParams();
  const [singleProduct, setSingleProduct] = useState({});
  const [qntyCount, setQntyCount] = useState(1);
  
  let API = `https://fakestoreapi.com/products/${id}`;
  const fetchAPI = async (url) => {
    try {
      const response = await fetch(url);
      const data = await response.json();
      setSingleProduct(data);
    }
    catch (error) {
      console.log(error)
    }
  }
  
  useEffect(() => {
    fetchAPI(API)
  }, [API, id]);

  const handleQntyDec = () => {
    if (qntyCount > 1) {
      setQntyCount(qntyCount - 1);
    }
  };
  
  const handleQntyInc = () => {
    setQntyCount(qntyCount + 1);
  };
  
  itemCountFun(qntyCount);
  
  // working on dublicate value
  console.log(handleDublicateItem(id));

  if (singleProduct.rating === undefined) {
    return (<></>);
  }

  return (
    <>
      <div className="ui_product_details">
        <Container>
          <Row>
            <Col md={6} sm={12}>
              <div className="product_left_column">
                <div class="card">
                  <img
                    src={singleProduct.image}
                    class="card-img-top"
                    alt={singleProduct.title}
                  />
                </div>
              </div>
            </Col>
            <Col md={6} sm={12}>
              <div className="product_right_column">
                <h1 className="section_title" style={{ textAlign: "left" }}>
                  {singleProduct.title}
                </h1>
                <div className="rating_star">
                  <DynamicStar rating={singleProduct.rating.rate} width={35} outlined />
                  <span>Rating {`(${singleProduct.rating.rate})`}</span>
                </div>
                <h1 className="product_price">${`${singleProduct.price}`}</h1>
                <div className="product_details">
                  <h4 className="details_title">Details Product</h4>
                  <p className="details_desc">{singleProduct.description}</p>
                </div>
                <div className="product_quantity">
                  <h5 className="details_title">Quantity</h5>
                  <div className="added_box">
                    <Button
                      type="button"
                      className="qty-btn"
                      onClick={handleQntyDec}
                    >
                      <i class="fa-solid fa-minus"></i>
                    </Button>
                    <input
                      type="text"
                      className="display-qty"
                      value={qntyCount}
                      disabled
                    />
                    <Button
                      type="button"
                      className="qty-btn"
                      onClick={handleQntyInc}
                    >
                      <i class="fa-solid fa-plus"></i>
                    </Button>
                  </div>
                  <h6>Add note</h6>
                </div>
                <div className="text-center mt-5">
                  <Button type="button" className="product_btn">
                    Wishlist
                  </Button>
                  &nbsp;&nbsp;&nbsp;
                  <Button
                    type="button"
                    className="product_btn"
                    onClick={() => handleAddToCart(singleProduct)}
                  >
                    Add to cart
                  </Button>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
      <ProductDescription />
      <NewsLetter />
    </>
  );
};

export default ProductDetails;
