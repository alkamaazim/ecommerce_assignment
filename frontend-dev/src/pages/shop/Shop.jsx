import React, { useEffect, useState } from "react";
import "../../styles/pages-style/pages-style.scss";
import { Container, Row, Col } from "react-bootstrap";
import ShopCard from "../../components/shopComponents/ShopCard";
import Accordian from "./Accordian";
import NewsLetter from "../../components/homeComponents/newsletter/NewsLetter";

const Shop = ({fetchProduct}) => {
  const searchData = fetchProduct;
  const [allProducts, setAllProducts] = useState(fetchProduct);
  const [catFilter, setCatFilter] = useState([]);
  const [filterVal, setFilterVal] = useState("");
  const [activeLinkColor, setActiveLinkColor] = useState({
    mens:"red",
    womens:"",
    jwelery:"",
    electronics:"",
  });

  useEffect(()=>{
    setCatFilter(allProducts);
  }, [allProducts])

  // search filter
  const handleInputEvent = (e) => {
    if (e.target.value === "") {
      setAllProducts(searchData);
    } else {
      const filterResult = searchData.filter((item) =>
        item.title.toLowerCase().includes(e.target.value.toLowerCase())
      );
      setAllProducts(filterResult);
    }
    setFilterVal(e.target.value);
  };

  // category filter
  const handleCategoryFilter = (data) => {
    if(data === 1){
      setActiveLinkColor({mens:"red", womens:"", jwelery:"", electronics:""})
      const data_filter = allProducts.filter((item)=>  item.category === "men's clothing");
      setCatFilter(data_filter)
    }
    else if(data === 2){
      setActiveLinkColor({mens:"", womens:"red", jwelery:"", electronics:""})
      const data_filter = allProducts.filter((item)=>  item.category === "women's clothing");
      setCatFilter(data_filter)
    }
    else if(data === 3){
      setActiveLinkColor({mens:"", womens:"", jwelery:"red", electronics:""})
      const data_filter = allProducts.filter((item)=>  item.category === "jewelery");
      setCatFilter(data_filter)
    }
    else if(data === 4){
      setActiveLinkColor({mens:"", womens:"", jwelery:"", electronics:"red"})
      const data_filter = allProducts.filter((item)=>  item.category === "electronics");
      setCatFilter(data_filter)
    }
  }

  return (
    <>
      <div className="ui_shop_wrapper">
        <Container>
          <Row>
            <Col md={3} sm={12}>
              <div>
                <div className="shop_search_box">
                  <input
                    className="form-control me-2 shop_search"
                    type="search"
                    placeholder="Search"
                    aria-label="Search"
                    onChange={(e) => {
                      handleInputEvent(e);
                    }}
                    value={filterVal}
                  />
                </div>
                <div className="price_range">
                  <div for="customRange1" className="form-label range_title">
                    <h4>Price</h4>
                    <p>Filter</p>
                  </div>
                  <input type="range" class="form-range" id="customRange1" />
                </div>
                <Accordian handleCategoryFilter={handleCategoryFilter} activeLinkColor={activeLinkColor} />
              </div>
            </Col>
            <div className="col-md-9 col-sm-12">
              <Row>
                {catFilter.map((item) => (
                  <ShopCard
                    key={item.id}
                    id={item.id}
                    rating={item.rating}
                    price={item.price}
                    image={item.image}
                    title={item.title}
                    category={item.category}
                  />
                ))}
              </Row>
            </div>
          </Row>
        </Container>
      </div>
      <NewsLetter />
    </>
  );
};

export default Shop;
