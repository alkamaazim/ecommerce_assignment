import React from 'react';

const Accordian = ({ handleCategoryFilter, activeLinkColor }) => {

    const categoryFilter1 = () => {
        handleCategoryFilter(1);
    }
    const categoryFilter2 = () => {
        handleCategoryFilter(2);
    }
    const categoryFilter3 = () => {
        handleCategoryFilter(3);
    }
    const categoryFilter4 = () => {
        handleCategoryFilter(4);
    }

  return (
    <>
        <div className='ui_accordian'>
            <div>
                <ul>
                    <li className='accordian_list' style={{color: activeLinkColor.mens}} onClick={categoryFilter1}>
                        <h5 className='list-item'>Men's Clothing</h5>
                        <h5><i class="fa fa-angle-right"></i></h5>
                    </li>
                    <li className='accordian_list' style={{color: activeLinkColor.womens}} onClick={categoryFilter2}>
                        <h5 className='list-item'>Women's Clothing</h5>
                        <h5><i class="fa fa-angle-right"></i></h5>
                    </li> 
                    <li className='accordian_list' style={{color: activeLinkColor.jwelery}} onClick={categoryFilter3}>
                        <h5 className='list-item'>Jwellery</h5>
                        <h5><i class="fa fa-angle-right"></i></h5>
                    </li> 
                    <li className='accordian_list' style={{color: activeLinkColor.electronics}} onClick={categoryFilter4}>
                        <h5 className='list-item'>Electroics</h5>
                        <h5><i class="fa fa-angle-right"></i></h5>
                    </li>  
                </ul>
            </div>
        </div>
    </>
  )
}

export default Accordian